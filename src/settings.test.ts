import sql from 'sqlite3';
import Bluebird from 'bluebird';

import { settingsMetadata } from './settings';
import { getDB } from './test/device_db';

async function genAsciiSettings(db: sql.Database) {
  const rows = await Bluebird.fromCallback<{ ASCIIName: string }[]>(cb => db.all(
    'SELECT DISTINCT ASCIIName FROM Data_Settings WHERE ASCIIName IS NOT NULL ORDER BY ASCIIName;',
    [], cb));
  return rows.map(row => row.ASCIIName);
}

let asciiSettings: string[];

beforeAll(async () => {
  asciiSettings = await genAsciiSettings(getDB());
}, 180 * 1000);

test('contains all the settings', () => {
  expect(Object.keys(settingsMetadata).sort()).toEqual(asciiSettings);
});

test('human name is not too long', () => {
  for (const setting of Object.values(settingsMetadata)) {
    if (setting.humanName) {
      expect(setting.humanName.length).toBeLessThanOrEqual(40);
    }
  }
});
