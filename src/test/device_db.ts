import { getLatestDatabaseFile } from '@zaber/tools';
import sql from 'sqlite3';

let db: sql.Database;
export const getDB = (): sql.Database => db;

beforeAll(async () => {
  const dbPath = await getLatestDatabaseFile();
  db = new sql.Database(dbPath, sql.OPEN_READONLY);
}, 1800 * 1000);
