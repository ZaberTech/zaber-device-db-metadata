# Zaber Device DB Metadata

Repository with additional information that does not quite fit into Device DB but is used in our software.

# Development

```
npm run build
npm run test
npm run lint
```

Checkout the review files in `review` folder.
