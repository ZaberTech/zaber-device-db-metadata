const fs = require('fs');
const path = require('path');
const ejs = require('ejs');

const { settingsMetadata, categoriesMetadata } = require('../lib');

async function main() {
  const data = {
    settingsMetadata,
    getCategory(setting) {
      let category = categoriesMetadata[setting.category];
      let name = category.humanName;
      while (category.parent in categoriesMetadata) {
        category = categoriesMetadata[category.parent];
        name = `${category.humanName} > ${name}`;
      }
      return name;
    }
  };

  const reviewPath = path.join(__dirname, '..', 'review');
  await fs.promises.mkdir(reviewPath, { recursive: true });

  const templateFile = path.join(__dirname, 'settings_review.ejs');
  const compiled = await ejs.renderFile(templateFile, data, {});

  const outputFile = path.join(reviewPath, 'settings_review.html');
  await fs.promises.writeFile(outputFile, compiled);
}

main().catch(err => {
  console.error(err);
  process.exit(1);
});
