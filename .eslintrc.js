module.exports = {
  parser: '@typescript-eslint/parser',
  env: {
    'es6': true,
    'node': true
  },
  extends: [
    '@zaber/eslint-config',
  ],
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
    project: './tsconfig.json',
  },
  ignorePatterns: [
    'generated/units/**/*.js',
  ],
};
