module.exports = {
  'env': {
    'es6': true,
    'node': true
  },
  extends: '@zaber/eslint-config/js',
  'parserOptions': {
    'ecmaVersion': 2018
  },
};
